    FROM python:3.11-alpine3.17

    WORKDIR /code

    COPY ./dist/*.tar.gz /code/
    RUN ls
    RUN apk add --no-cache tar
    RUN tar -xvf *.tar.gz
    RUN pip install -r /code/apishopping-0.0.1/requirements.txt

    WORKDIR /code/apishopping-0.0.1
    RUN ls
    EXPOSE 8001/tcp
    CMD ["uvicorn", "app.project.main:app", "--host", "0.0.0.0", "--port", "8001"]



